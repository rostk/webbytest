import { Provider } from 'react-redux';
import { MoviesProvider } from './context/contex';
import store from './store';
import MoviesList from './movies-list/movies-list'
function App() {


  return (
    <>
      <Provider store={store}>
        <MoviesProvider>
          <MoviesList />
        </MoviesProvider>
      </Provider>
    </>
  );
}

export default App;
