import { useMoviesContext } from '../context/contex';
import { Modal, Button, InputGroup, FormControl, Dropdown, DropdownButton, Form } from 'react-bootstrap';
import { useState, useRef } from 'react';
import './App.css';
function MovieList() {
    const { movies, addMovie, onDelete, getById, fetchData, loadFile, search } = useMoviesContext();
    const [show, setShow] = useState(false);
    const [cardShow, setCardShow] = useState(false);
    const [format, setFormat] = useState('DVD')
    const [event, setEvent] = useState('');
    const [currentCard, setCurrentCard] = useState('');
    const [onSwitch, setSwitch] = useState('false');
    const releaseRef = useRef();
    const titleRef = useRef();
    const starsRef = useRef();
    const inputRef = useRef();
    const [error, setError] = useState([]);

    const handleClose = () => {
        setShow(false);
        setError('');
        setError([]);
    }
    const handleCardClose = () => {
        setCurrentCard('')
        setCardShow(false)
    }

    async function handleCardShow(id) {
        const card = await getById(id);
        setCurrentCard(card);
        setCardShow(true);
    }
    const handleShow = () => setShow(true);
    const changeFormat = (e) => setFormat(e.target.innerHTML);

    async function onCreate(e) {
        e.preventDefault();
        const body = { title: titleRef.current.value, release: releaseRef.current.value, format, stars: [...starsRef.current.value.split(',')] }
        const data = await addMovie(body);
        console.log(body.stars[0] === '')
        if (data.errors) {
            return setError(data.errors)
        }
        setError('');
        setShow(false);
        setEvent(data);
    }
    async function onLoad() {
        const data = await loadFile();
        if (data) {
            setEvent(data.message);
        }
    }

    async function onQuery(e) {
        e.preventDefault();
        await search(inputRef.current.value)
    }
    async function sortMovies(e) {
        setSwitch(!onSwitch);
        await fetchData(onSwitch)
    }
    async function onDeleteMovie(e, id) {
        e.preventDefault();
        const msg = await onDelete(id);
        setEvent(msg);
    }
    const content = movies.map(item =>
        <li className="card movie_card" key={item._id} >
            <div className="card-body" onClick={() => handleCardShow(item._id)}>
                <h5 className="card-title" id='title'>{item.title}</h5>
                <span className="movie_info">{item.release}</span>
            </div>
            <i className="material-icons play_button" data-toggle="tooltip" data-placement="bottom" title="Delete" onClick={(e) => onDeleteMovie(e, item._id)}>delete_sweep</i>
        </li>)
    console.log(movies)
    return (
        <div className="container mt-5">
            <span className="d-none">{event && setTimeout(() => setEvent(''), 3000)}</span>
            {event && <div className="alert alert-light" role="alert">
                {event}
            </div>}
            <h2 className="text-center">Test task WebbyLab</h2>
            <Form.Group className=" rounded-4" >
                <Form.Control size="lg" id="search" type="text" placeholder="Search" ref={inputRef} onChange={onQuery} />
            </Form.Group>
            <div className="row">
                <Form>
                    <Form.Check
                        type="switch"
                        id="custom-switch"
                        label="Sort movies by alphabet"
                        onClick={sortMovies}
                    />
                </Form>
                <label className="ml-5 " id='loadFile' onClick={() => onLoad()}>Load File: <i className="fas fa-download "></i></label>
            </div>

            <div className="row justify-content-center">
            </div>
            <ul className='list row'>
                {!movies[0] ? <div>No movie found...</div> : content}
                <li className="add_btn">
                    <i className="material-icons add-i" id="add" onClick={handleShow}>add_circle_outline</i>
                </li>
            </ul>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Create release</Modal.Title>
                
                </Modal.Header>
                <Modal.Body>
                    <div className='d-flex justify-content-center text-danger'>{error.find(el => el === 'Movie already exist,please try again')}</div>
                    <span className='text-danger'>{error.find(el => el === 'Title length is incorrect')}</span>
                    <InputGroup size="sm" className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputGroup-sizing-sm">Title</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" ref={titleRef} />
                    </InputGroup>
                    <span className='text-danger'>{error.find(el => el === 'Release field must be a year')}</span>
                    <InputGroup size="sm" className="mb-3">
                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputGroup-sizing-sm" type="number">Release</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" ref={releaseRef} />
                    </InputGroup>
                    <InputGroup className="mb-3">
                        <DropdownButton
                            as={InputGroup.Prepend}
                            variant="outline-secondary"
                            title={format}
                            id="input-group-dropdown-1"
                        >
                            <Dropdown.Item onClick={changeFormat}>VHS</Dropdown.Item>
                            <Dropdown.Item onClick={changeFormat}>DVD</Dropdown.Item>
                            <Dropdown.Item onClick={changeFormat}>Blu-Ray</Dropdown.Item>
                        </DropdownButton>

                    </InputGroup>     <span className='text-danger'>{error.find(el => el === 'Stars field is empty Empty')}</span>
                    <InputGroup size="sm" className="mb-3">

                        <InputGroup.Prepend>
                            <InputGroup.Text id="inputGroup-sizing-sm">Stars</InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" ref={starsRef} />
                    </InputGroup>

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose} id="close">
                        Close
        </Button>
                    <Button variant="primary" onClick={onCreate}>
                        Save
        </Button>
                </Modal.Footer>
            </Modal>


            <Modal show={cardShow} onHide={handleCardClose}>

                <Modal.Header closeButton>
                    <Modal.Title>{currentCard.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>Relese Year : {currentCard.release}</div>
                    <div>Format : {currentCard.format}</div>
                    <div> Stars : {currentCard.stars}</div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCardClose}>
                        Close
        </Button>

                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default MovieList;
