const MOVIES_REQUEST = () => {
    return {
        type: 'MOVIES_REQUEST'
    }
};

const FETCH_MOVIES = movies => {
    return {
        type: 'FETCH_MOVIES',
        payload: movies
    }
}
const ADD_MOVIE = movie => {
    return {
        type: 'ADD_MOVIE',
        payload: movie
    }
}
const DELETE_MOVIE = id => {
    return {
        type: 'DELETE_MOVIE',
        payload: id
    }
}
const LOAD_FILE = movies => {
    return {
        type: 'LOAD_FILE',
        payload: movies
    }
}
export {
    MOVIES_REQUEST,
    FETCH_MOVIES,
    ADD_MOVIE,
    DELETE_MOVIE,
    LOAD_FILE
}