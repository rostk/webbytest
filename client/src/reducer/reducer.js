
const deleteMovie = (state, payload) => {
    const idx = state.movies.findIndex(({ _id }) => _id === payload)
    return {
        ...state,
        movies: [
            ...state.movies.slice(0, idx),
            ...state.movies.slice(idx + 1)
        ],
        loading: false
    };
}




const initialState = {
    movies: [],
    loading: true,
    error: null
}
const reducer = (state = initialState, action) => {

    switch (action.type) {
        case 'MOVIES_REQUEST':
            return {
                ...state,
                loading: true,
                error: null
            };
        case 'FETCH_MOVIES':
            return {
                ...state,
                movies: [...action.payload],
                loading: false
            }
        case 'ADD_MOVIE':
            return {
                ...state,
                movies: [action.payload, ...state.movies],
                loading: false
            }
        case 'LOAD_FILE':
            return {
                ...state,
                movies: [...action.payload, ...state.movies],
                loading: false
            }
        case 'DELETE_MOVIE':
            return deleteMovie(state, action.payload);

        default:
            return state;
    }
}
export default reducer;