export default class MoviesSevice {

    fetchResource = async (url, method) => {
        const res = await fetch(url, {
            method,
        })

        const data = await res.json();
        console.log(data.message)
        if (!data) {
            return;
        }

        return data;
    }

    postResource = async (url, method, body = {}) => {
        const res = await fetch(url, {
            method,
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json'
            }
        })

        const data = await res.json();
        if (!data) {
            return;
        }
        return data;
    }

    fetchMovies = async () => {
        return await this.fetchResource('/api', 'GET');
    }
    fetchSortedMovies = async () => {
        return await this.fetchResource('/api/sort', 'GET');
    }
    getList = async () => {
        return await this.fetchResource('/api/list', 'GET');
    }
    addMovie = async (body) => {
        return await this.postResource('/api/create', 'POST', body);
    }
    deleteMovie = async (id) => {
        return await this.postResource(`/api/delete/${id}`, 'DELETE');
    }

    getById = async (id) => {
        return await this.fetchResource(`/api/movies/${id}`);
    }

}