import { useSelector, useDispatch } from 'react-redux';
import React, { useContext, useEffect, useCallback, useState } from "react";
import { MOVIES_REQUEST, FETCH_MOVIES, ADD_MOVIE, DELETE_MOVIE, LOAD_FILE } from '../actions/actions';
import MoviesSevice from '../service/service'
const Context = React.createContext()

export function useMoviesContext() {
  return useContext(Context)
}

export function MoviesProvider({ children }) {
  const state = useSelector(state => state);
  const [movies, setMovies] = useState([]);
  const dispatch = useDispatch();
  const request = useCallback(() => dispatch(MOVIES_REQUEST()), [dispatch]);
  const fetch = useCallback((movies) => dispatch(FETCH_MOVIES(movies)), [dispatch]);
  const createMovie = useCallback((movies) => dispatch(ADD_MOVIE(movies)), [dispatch]);
  const downFile = useCallback((movies) => dispatch(LOAD_FILE(movies)), [dispatch]);
  const deletedMovie = useCallback((id) => dispatch(DELETE_MOVIE(id)), [dispatch]);
  const service = new MoviesSevice();

  const fetchData = useCallback(async (toggle = false) => {
    if (toggle) {
      const data = await service.fetchSortedMovies();
      fetch(data);
      setMovies(data);
      return;
    }
    const data = await service.fetchMovies();
    setMovies(data);
    fetch(data);
  }, [fetch]);

  async function addMovie(body) {
    const newMovie = await service.addMovie(body);
    if (newMovie.errors) {
      return newMovie;
    }
    createMovie(newMovie.movie);
    return newMovie.message;
  }
  async function onDelete(id) {
    const msg = await service.deleteMovie(id);
    deletedMovie(id);
    console.log(msg)
    return msg.message;
  }
  async function getById(id) {
    const movie = await service.getById(id);
    return movie;
  }
  async function loadFile() {
    const data = await service.getList();
    console.log(data)
    if(data.message){
      return data;
    }
    downFile(data)
  }
  function search(term) {
    if (term.length === 0) return fetch(movies);
    console.log(movies)
    const data = movies.filter((item) => {
      let i = 0;
      console.log(item.stars.map(str => str.indexOf(term) > -1)   ) 
      const res =  term.toLowerCase() ? item.title.toLowerCase().indexOf(term.toLowerCase()) > -1 
      || item.title.indexOf(term) > -1 
     || item.stars.filter(str => str.indexOf(term) > -1)[i] : false;
        i++;
        return res;
    })
    fetch(data)
  }
  useEffect(() => {
    fetchData()
    console.log('useEffect');
  }, [fetchData])

  const value = {
    movies: state.movies,
    addMovie,
    onDelete,
    getById,
    fetchData,
    loadFile,
    search
  }

  return (
    <Context.Provider value={value}>
      {state.loading ? <div>Loading...</div> : children}
    </Context.Provider>
  )
}