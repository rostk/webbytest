const { Router } = require('express');
const Movies = require('../models/movies.model');
const router = Router();
const { movieValidator } = require('../utils/validators')
const { validationResult } = require('express-validator');
const List = require('../fs');

router.get('/', async (req, res) => {
    try {
        const data = await Movies.find();
        res.status(200).json(data);
    } catch (e) {
        res.status(500).json({ message: "Something goes wrong" })
    }
});
router.get('/sort', async (req, res) => {
    try {
        const data = await Movies.find().sort({"title":1});
        res.status(200).json(data);
    } catch (e) {
        res.status(500).json({ message: "Something goes wrong" })
    }
});

router.post('/create', movieValidator, async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array().map(m => m.msg),
            message: 'Incorrect data'
        })
    }
    try {
        const { title, release, format, stars } = req.body;
        const movie = new Movies({
            title,
            release,
            format,
            stars
        });
        await movie.save()
        res.status(201).json({"message" : "Movies created",  movie});
    } catch (e) {
        res.status(500).json({ message: "Something goes wrong" })
    }
});

router.get('/list', async (req, res) => {
    try {
        const list = await List();
        if(list.name === 'BulkWriteError'){
            return res.status(422).json({ message: "Some items from file is already exist delete all files data" })
        }
        res.status(200).json(list);
    } catch (error) {
        res.status(500).json({ message: "Something goes wrong" })
    }

})

router.get('/movies/:id', async (req, res) => {
    try {
        const movie = await Movies.findById(req.params.id);
        res.status(200).json(movie);
    } catch (e) {
        res.status(500).json(e);
    }
});


router.get('/search/content', async (req, res) => {  //search content by query like (http://localhost:5000/api/movies/search/content?search={exp})
    try {
        const search = await Movies.find(
            {
                $or: [      //return movies whitch includes expression in content(title or stars)
                    { title: { "$regex": req.query.search, "$options": "i" } },
                    { stars: { "$regex": req.query.search, "$options": "i" } }
                ]
            });
        res.status(200).json(search);
    } catch (error) {
        res.status(500).json({ message: 'Something goes wrong' });
    }
});
router.delete('/delete/:id', async (req, res) => {
    try {
        await Movies.findByIdAndDelete(req.params.id)
        res.status(200).json({ message: 'Movie deleted' })
    } catch (e) {
        res.status(500).json({ message: e.message })
    }

})


module.exports = router;




