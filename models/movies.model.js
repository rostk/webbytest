const { Schema, model } = require('mongoose');

const movies = new Schema({
    title: {
        type: String,
        required: true,
        default: "",
        unique: true
    },
    release: {
        type: String,
        required: true
    },
    format: {
        type: String,
        required: true
    },
    stars: {
        type: Array,
        required: true,
        default: []
    },


})

module.exports = model('Movies', movies);