const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const keys = require('./config/keys.dev');


app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api', require('./routes/movies.route'));


async function serverStart(){
    try{
        await mongoose.connect(keys.MONGO_KEY, {useNewUrlParser : true, useUnifiedTopology: true, useFindAndModify: false});
        app.listen(keys.PORT, ()=> console.log(`server has been started on port ${keys.PORT}`));
    }catch(e){
        console.log(e);
        process.exit(1);
    }
    
}

serverStart();