const puppeteer = require('puppeteer');


let page, browser;



test('should check all functions ', async () => {
    browser = await puppeteer.launch({
        headless: false,
        slowMo: 120,
        args: ['--window-size=1920,1080']
    });
    page = await browser.newPage();
    await page.goto(
        'http://localhost:3000/'
    );
    await page.evaluate(
        () => {
            return fetch('http://localhost:3000/api/create', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ title: 'Die Hard', release: '1994', format: 'DVD', stars: ['bruce willis', 'allan rikman'] })
            }).then(res => res.json());
        }
    )
    await page.goto('http://localhost:3000/');
    const title = await page.$eval('#title', el => el.innerHTML);
    expect(title)
    await page.click('#loadFile');
    const title2 = await page.$eval('#title', el => el.innerHTML);
    expect(title2);
    await page.click('#custom-switch');
    await page.click('#custom-switch');
    await page.$eval('#search', el => el.value = 'The Stin');
    await page.click('#search');
    await page.keyboard.press('g');
    await page.close();
    await browser.close();

}, 200000);
