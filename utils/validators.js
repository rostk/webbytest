const Movies = require('../models/movies.model');
const { body } = require('express-validator');

exports.movieValidator = [
    body('title', 'Title length is incorrect').not().isEmpty().isLength({ min: 2, max: 16 }).custom(async(value, {req})=>{
        try{
            const movie = await Movies.find({title : value});
            if(movie[0]){
                return Promise.reject("Movie already exist,please try again")
            }
        }catch(e){
            console.log(e);
        }
    }),
    body('release', 'Release field must be a year').not().isEmpty().isNumeric().isLength({ min: 4, max: 4 }),
    body('stars', 'Stars field is empty Empty').not().isEmpty()
]