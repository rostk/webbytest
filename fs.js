const fs = require('fs');
const path = require('path');
const Movies = require('./models/movies.model');
function getField(lines) {
    return lines.shift().split(':')[1];
}
function readFromFile() {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(__dirname, './', 'moviesFile.txt'), 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {

                var res = [];
                var lines = data.split("\r\n");

                while (lines.length > 0) {
                    res.push({
                        title: getField(lines),
                        release: getField(lines),
                        format: getField(lines),
                        stars: getField(lines).split(',')
                    });

                    if (lines[0] == '') lines.shift();
                }

                resolve(res)

            }
        })
    })
}


async function insertDataFromFile() {
    try {
        const data = await readFromFile();
        return await Movies.insertMany(data);
    } catch (e) {
        return e;
    }

}



module.exports = insertDataFromFile;